﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
namespace Office
{
    public partial class Form1 : Form
    {   
        //手动导入文件路径
        public string inputpath = "";
        //服务器文件路径
        public string ftppath = "http://192.168.10.217:8002/ceshi.xls";
        //本地默认储存路径
        public string cpath =GetAppsettingStr("LocalPath");
        
        //本地文件地址
        public string openpath = "";
        //服务器存放地址
        public string iispath = GetAppsettingStr("MDToolUpLoadFiless");
        public Form1()
        {
            InitializeComponent();

            IsRegister();//机器是否注册dsoframer.ocx插件
            CreateDir(cpath);//创建本地文件夹

            //将文件下载到本地
            openpath=  Download(ftppath, cpath);

            //本地打开方式
            axFramerControl1.Open(openpath);

           //返回应用程序打开状态
            MDToolWebService.WebService1SoapClient mt = new MDToolWebService.WebService1SoapClient();
            string ipconfig = GetLocalIp();
            bool toolstate= mt.SetClientState(ipconfig,1);

        }


        /// <summary>
        /// 导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog op = new OpenFileDialog();
                if (op.ShowDialog() == DialogResult.OK)
                {
                    inputpath = op.FileName;
                    if (IsOffice(inputpath)) { this.axFramerControl1.Open(inputpath); }
                    else { MessageBox.Show("请选择指定格式的文档！"); }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("异常:" + ex.ToString());
            }
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Title = "请选择保存文件路径";
                saveFile.Filter = "Excel(*.xls)|*.xlsx|Word(*.doc)|*.docx";
                saveFile.OverwritePrompt = true;  //是否覆盖当前文件
                saveFile.RestoreDirectory = true;  //还原目录
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    //文档另存为
                    //Download(ftppath, saveFile.FileName);
                    MessageBox.Show("另存文档成功!");
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// 保存同步到服务器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                axFramerControl1.Close();
                UploadFile(openpath, iispath);
            }
            catch (Exception ex)
            {

                MessageBox.Show("错误:" + ex);
            }
        }

        /// <summary>
        /// 验证是否为文档文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsOffice(string path)
        {
            bool istrue = false;
            if (path == "") MessageBox.Show("请选择文档");
            string aLastName = path.Substring(path.LastIndexOf(".") + 1, (path.Length - path.LastIndexOf(".") - 1));
            string[] officearry = new string[] { "xls", "xlsx", "doc", "docx", "ppt" };
            for (int i = 0; i < officearry.Length; i++)
            {
                if (aLastName == officearry[i]) istrue = true;
            }
            return istrue;
        }

        /// <summary>
        /// 下载文件方法
        /// </summary>
        /// <param name="serverPath">服务器地址文件</param>
        /// <param name="filePath">存放的路径</param>
        public String Download(string serverPath, string filePath)
        {
            WebClient client = new WebClient();
            string fileName = serverPath.Substring(serverPath.LastIndexOf("/") + 1);//被下载的文件名
            string path = filePath + fileName;//另存为地址
            try
            {
                WebRequest myre = WebRequest.Create(serverPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return path;
            }
            try
            {
                client.DownloadFile(serverPath, fileName);
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader r = new BinaryReader(fs);
                byte[] mbyte = r.ReadBytes((int)fs.Length);
                FileStream fstr = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                fstr.Write(mbyte, 0, (int)fs.Length);
                fstr.Close();
                return path;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return path;
            }
            
        }

        /// <summary>

        /// 上传文件方法
        /// </summary>
        /// <param name="filePath">本地文件所在路径（包括文件）</param>
        /// <param name="serverPath">文件存储服务器路径（包括文件）</param>
        public void UploadFile(string filePath, string serverPath)
        {
            
            try
            {
                //创建WebClient实例
                WebClient webClient = new WebClient();
                webClient.UploadFileCompleted += WebClient_UploadFileCompleted;
                webClient.Credentials = CredentialCache.DefaultCredentials;
                webClient.Headers.Add("Content-Type", "application/form-data");//注意头部必须是form-data
                webClient.QueryString["HostNumebr"] = "30048";
                webClient.UploadFileAsync(new Uri(iispath), "POST", filePath);
                //string getPath = Encoding.GetEncoding("UTF-8").GetString(responseArray);

            }
            catch (Exception ex)
            {
                
            }
        }

        private void WebClient_UploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                
            }
            else
            {
                
            }
        }


        /// <summary>
        /// 获取本机的IP地址
        /// </summary>
        /// <returns></returns>
        public string GetLocalIp()
        {
            string AddressIP = string.Empty;
            foreach (IPAddress _IPAddress in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            {
                if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    AddressIP = _IPAddress.ToString();
                }
            }
            return AddressIP;
        }

        /// <summary>
        /// 注册dsoframer插件
        /// </summary>
        public static void IsRegister()
        {
            string curPath = System.AppDomain.CurrentDomain.BaseDirectory;
            try
            {
                if (Directory.Exists(@"C:\Windows\SysWOW64") && File.Exists(@"C:\Windows\SysWOW64\" + "dsoframer.ocx") == false)
                {
                    File.Copy(curPath + "dsoframer.ocx", @"C:\Windows\SysWOW64\" + "dsoframer.ocx");
                    using (System.Diagnostics.Process p = new System.Diagnostics.Process())
                    {
                        p.StartInfo.FileName = "cmd.exe";
                        p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
                        p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
                        p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
                        p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
                        p.StartInfo.CreateNoWindow = true;//不显示程序窗口
                        p.Start();//启动程序
                        //向cmd窗口发送输入信息
                        p.StandardInput.WriteLine(@"regsvr32 C:\Windows\SysWOW64\dsoframer.ocx");
                        p.StandardInput.AutoFlush = true;
                        p.StandardInput.WriteLine("exit");
                        p.WaitForExit();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                if (Directory.Exists(@"C:\Windows\System32") && File.Exists(@"C:\Windows\System32\" + "dsoframer.ocx") == false)
                {
                    File.Copy(curPath + "dsoframer.ocx", @"C:\Windows\System32\" + "dsoframer.ocx");
                    using (System.Diagnostics.Process p = new System.Diagnostics.Process())
                    {
                        p.StartInfo.FileName = "cmd.exe";
                        p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
                        p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
                        p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
                        p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
                        p.StartInfo.CreateNoWindow = true;//不显示程序窗口
                        p.Start();//启动程序
                        //向cmd窗口发送输入信息
                        p.StandardInput.WriteLine(@"regsvr32 C:\Windows\System32\dsoframer.ocx");
                        p.StandardInput.AutoFlush = true;
                        p.StandardInput.WriteLine("exit");
                        p.WaitForExit();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("22: " + ex.Message);
            }

        }

        /// <summary>
        /// 根据文件夹全路径创建文件夹
        /// </summary>
        /// <param name="LogPath"></param>
        public static void CreateDir(string path)
        {
            if (Directory.Exists(path))
            {
                return;
            }
            else
            {
                Directory.CreateDirectory(path);
                return;
            }
        }



        /// <summary>
        /// 获取配置文件里appsettings的数据
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetAppsettingStr(string str)
        {
            AppSettingsReader appReader = new AppSettingsReader();
            return appReader.GetValue(str, typeof(string)).ToString();
        }



    }
}
